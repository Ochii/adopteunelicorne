<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnicornsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unicorns', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('name');
            $table->integer('age');
            $table->string('gender');
            $table->bigInteger('idFarm')->unsigned()->nullable();
            $table->foreign('idFarm')->references('id')->on('farms')->onDelete('SET NULL');
            $table->string('price')->nullable();
            $table->dateTime('firstReproDate')->nullable();
            $table->dateTime('secReproDate')->nullable();
            $table->bigInteger('idUser')->unsigned()->nullable();
            $table->foreign('idUser')->references('id')->on('users')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unicorns');
    }
}

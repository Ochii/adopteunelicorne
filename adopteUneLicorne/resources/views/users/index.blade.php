@extends('layouts.app')

@section('content')
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Mail</th>
        <th scope="col">Role</th>
        <th scope="col">Supprimer</th>
      </tr>
    </thead>
    <tbody>
        @foreach($users as $user)
        <tr>
            <th scope="row">{{ $user->id }}</th>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                <form action="{{ route('users.update', $user->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                <div class="form-group">
                    <select class="form-control" id="role" name="role">
                        <option value="{{ $user->role }}">Actuel : {{ $user->role }}</option>
                        <option value="admin">Modifier : admin</option>
                        <option value="user">Modifier : user</option>
                        <option value="guest">Modifier : guest</option>
                    </select>
                </div>
                <button class="btn btn-warning" type="submit">Modifier</button>
            </form>
            </td>
            <td>
            <form action="{{ route('users.destroy') }}" method="POST">
                @csrf
                @method('DELETE')
                <input type="hidden" name="id" value="{{ $user->id }}">
                <button class="btn btn-danger" type="submit">Supprimer</button>
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <h1> {{$user->name }} </h1>
    <ul> Propriétaire des élevages suivants :
    @foreach ($user->farms as $farm)
        <li><a href="{{ route('farms.show', $farm->id) }}">{{ $farm->name }}</a></li>
    @endforeach
    </ul>
    <br>
    <ul> Propriétaire des licornes suivantes :
        @foreach ($user->unicorns as $unicorn)
            <li><a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a></li>
        @endforeach
        </ul>
</div>
@endsection

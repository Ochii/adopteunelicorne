@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1><a href="{{ route('farms.index') }}" data-toggle="tooltip" data-placement="bottom" title="Voir tous les élevages">Les élevages</a></h1>
    </div>
    <div class="row justify-content-center">
        @if (!is_null($farms))
        @foreach ($farms as $farm)
        <div class="col-2">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    <a href="{{ route('farms.show', $farm->id) }}">{{ $farm->name }}</a>
                  </div>
                <div class="card-body">
                    <h5 class="card-title">Lieu : {{ $farm->loc }}</h5>
                    <p class="card-text">Descriptif : {{ $farm->content }}</p>
                    <a href="{{ route('farms.show', $farm->id) }}" class="btn btn-primary">En savoir plus</a>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $farm->idUser) }}" title="{{ $farm->user->name }}">{{ $farm->user->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas d'élevage enregistré</div>
        @endif

    </div>

    <div class="row justify-content-between">
        <h1><a href="{{ route('unicorns.index') }}" data-toggle="tooltip" data-placement="bottom" title="Voir toutes les licornes">Les licornes</a></h1>
    </div>
    <div class="row justify-content-around">
        @if (!is_null($unicorns))
        @foreach ($unicorns as $unicorn)
        <div class="col-2">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    <a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a>
                  </div>
                <div class="card-body">
                    <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                    @if (!is_null($unicorn->price))
                        <p class="card-text">Prix de vente : {{ $unicorn->price }} €</p>
                    @else
                        <p class="card-text">Licorne uniquement pour reproduction</p>
                    @endif
                    <p class="card-text">1ère date de reproduction : <li>{{ $unicorn->firstReproDate }}</li></p>
                    <p class="card-text">2ème date de reproduction : <li>{{ $unicorn->secReproDate }}</li></p>
                    <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary">En savoir plus</a>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $unicorn->idUser) }}" title="{{ $unicorn->user->name }}">{{ $unicorn->user->name }}</a></h6>
                    <h6 class="card-subtitle mb-2 text-muted">Elevage : <a href="{{ route('farms.show', $unicorn->idFarm) }}" title="{{ $unicorn->farm->name }}">{{ $unicorn->farm->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas de licorne enregistrée</div>
        @endif

    </div>
</div>
@endsection

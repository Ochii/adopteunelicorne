@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row align-items-center">
        <div class="col-10 alert alert-dark" role="alert">
            <h1>Les licornes </h1>
        </div>
        <div class="col-2">
            <a class="btn btn-success btn-lg" href="{{ route('unicorns.create') }}">Ajouter une licorne</a>
        </div>
    </div>
    <br>
    <div class="col-12 alert alert-light" role="alert">
        <h3>A vendre :</h3>
    </div>
    <div class="row justify-content-center" style="height: 150px;">
        @if (!is_null($unicornsToSell))
        @foreach ($unicornsToSell as $unicorn)
        <div class="col-2" style="height: 100%;">
            <div class="card" style="width: 18rem; height: 100%;">
                <div class="card-header">
                    <a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                    @if (!is_null($unicorn->price))
                        <p class="card-text">Prix de vente : {{ $unicorn->price }} €</p>
                    @else
                        <p class="card-text">Licorne uniquement pour reproduction</p>
                    @endif

                    <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary">En savoir plus</a>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $unicorn->idUser) }}" title="{{ $unicorn->user->name }}">{{ $unicorn->user->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas de licorne enregistrée</div>
        @endif

    </div>
    <br>
    <div class="col-12 alert alert-light" role="alert">
        <h3>Pour reproduction :</h3>
    </div>
    <div class="row justify-content-center" style="height: 250px;">
        @if (!is_null($unicornsToRepro))
        @foreach ($unicornsToRepro as $unicorn)
        <div class="col-2" style="height: 100%;">
            <div class="card" style="width: 18rem; height: 100%;">
                <div class="card-header">
                    <a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                    <p class="card-text">1ère date de reproduction : <li>{{ $unicorn->firstReproDate }}</li></p>
                    <p class="card-text">2ème date de reproduction : <li>{{ $unicorn->secReproDate }}</li></p>
                    <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary">En savoir plus</a>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $unicorn->idUser) }}" title="{{ $unicorn->user->name }}">{{ $unicorn->user->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas de licorne enregistrée</div>
        @endif

    </div>
    <br>
    <div class="col-12 alert alert-light" role="alert">
        <h3>Sans status actuellement :</h3>
    </div>
    <div class="row justify-content-center" style="height: 250px;">
        @if (!is_null($unicornNoStatus))
        @foreach ($unicornNoStatus as $unicorn)
        <div class="col-2" style="height: 100%;">
            <div class="card" style="width: 18rem; height: 100%;">
                <div class="card-header">
                    <a href="{{ route('unicorns.show', $unicorn->id) }}">{{ $unicorn->name }}</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                    <p class="card-text">1ère date de reproduction : <li>{{ $unicorn->firstReproDate }}</li></p>
                    <p class="card-text">2ème date de reproduction : <li>{{ $unicorn->secReproDate }}</li></p>
                    <a href="{{ route('unicorns.show', $unicorn->id) }}" class="btn btn-primary">En savoir plus</a>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $unicorn->idUser) }}" title="{{ $unicorn->user->name }}">{{ $unicorn->user->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas de licorne enregistrée</div>
        @endif

    </div>
</div>
@endsection

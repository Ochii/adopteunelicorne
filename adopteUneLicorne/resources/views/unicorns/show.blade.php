@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    {{ $unicorn->name }}
                </div>
                <div class="card-body">
                    <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                    @if (!is_null($unicorn->price))
                        <p class="card-text">Prix de vente : {{ $unicorn->price }} €</p>
                    @else
                        <p class="card-text">Prix de vente : Aucun</p>
                    @endif
                    <p class="card-text">1ère date de reproduction : <li>{{ $unicorn->firstReproDate }}</li></p>
                    <p class="card-text">2ème date de reproduction : <li>{{ $unicorn->secReproDate }}</li></p>
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('users.show', $unicorn->idUser) }}" title="{{ $unicorn->user->name }}">{{ $unicorn->user->name }}</a></h6>
                    <h6 class="card-subtitle mb-2 text-muted">Elevage : <a href="{{ route('farms.show', $unicorn->idFarm) }}" title="{{ $unicorn->farm->name }}">{{ $unicorn->farm->name }}</a></h6>
                </div>
            @if(Auth::id() == $unicorn->idUser)
            <div class="row justify-content-around">
                <a class="btn btn-warning" href="{{ route('unicorns.edit', $unicorn->id) }}">modifier</a>
                <form action="{{ route('unicorns.destroy') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="id" value="{{ $unicorn->id }}">
                    <button class="btn btn-danger" type="submit">Supprimer</button>
                </form>
            </div>
            @endif
            <br>
            <a href="{{ route('unicorns.index') }}" class="btn btn-secondary" title="Retour a la liste">Retour à la liste</a>
            <br>
            @if($unicorn->price && $unicorn->idUser != Auth::id())
                <a href="{{ route('paiement', $unicorn->id) }}" class="btn btn-success" title="acheter">Acheter la licorne</a>
            @endif
        </div>
    </div>
</div>

@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Modification <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour a la home">Retour à l'accueil</a></h1>
    <form action="{{ route('unicorns.update', $unicorn->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nom de la licorne</label>
            <input class="form-control" id="name" type="text" name="name" value="{{ $unicorn->name }}">
        </div>
        <div class="form-group">
            <label for="age">Age</label>
            <input class="form-control" id="age" type="number" name="age" value="{{ $unicorn->age }}">
        </div>
        <div class="form-group">
            <label for="gender">Genre</label>
            <select class="form-control" id="gender" name="gender">
                <option value="{{ $unicorn->gender }}">Valeur actuelle : {{ $unicorn->gender }}</option>
                <option value="male">male</option>
                <option value="femelle">femelle</option>
            </select>
        </div>
        <div class="form-group">
            <label for="idUser">Elevage</label>
            <select class="form-control" id="idFarm" name="idFarm">
                <option value="{{ $unicorn->idFarm }}">{{ $unicorn->farm->name }}</option>
                @foreach($farms as $farm)
                    <option value="{{ $farm->id }}">{{ $farm->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="firstReproDate">Première date de reproduction possible</label>
            <input class="form-control" id="firstReproDate" type="date" name="firstReproDate" value="{{ $unicorn->firstReproDate }}">
        </div>
        <div class="form-group">
            <label for="secReproDate">Deuxième date de reproduction possible</label>
            <input class="form-control" id="secReproDate" type="date" name="secReproDate" value="{{ $unicorn->secReproDate }}">
        </div>
        <div class="form-group">
            <label for="price">Prix de vente</label>
            <div class="input-group">
                <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control currency" id="price" name="price" value="{{ $unicorn->price }}"/>
                <div class="input-group-append">
                    <span class="input-group-text">€</span>
                  </div>
            </div>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Enregistrer</button>
    </form>
</div>
@endsection

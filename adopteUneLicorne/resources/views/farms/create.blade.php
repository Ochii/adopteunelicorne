@extends('layouts.app')

@section('content')

<div class="container">
    <h1>Création d'un élevage <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour a la home">Retour à l'accueil</a></h1>
    <form action="{{ route('farms.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Nom de l'élevage</label>
            <input class="form-control" id="name" type="text" name="name">
        </div>
        <div class="form-group">
            <label for="loc">Localisation</label>
            <input class="form-control" id="loc" type="text" name="loc">
        </div>
        <div class="form-group">
            <label for="descr">Descriptif</label>
            <input class="form-control" id="descr" type="text" name="descr">
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Enregistrer</button>
    </form>
</div>
@endsection

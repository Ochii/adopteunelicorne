@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (!is_null($farm))
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    {{ $farm->name }}
                  </div>
                <div class="card-body">
                    <h5 class="card-title">Lieu : {{ $farm->loc }}</h5>
                    <p class="card-text">Descriptif : {{ $farm->content }}</p>
                    @if(!$unicorns)
                        <p>pas de licorne dans cet élevage</p>
                    @else
                        <p class="card-text">
                            <ul> Licornes à vendre
                                @if(is_null($unicorns))
                                    <li>Aucune</li>
                                @endif
                                @foreach($unicorns as $unicorn)
                                    @if(!is_null($unicorn->price))
                                        <li> <a href="{{ route('unicorns.show', $unicorn->id) }}"> {{ $unicorn->name }} ({{ $unicorn->gender }})</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </p>
                        <p class="card-text">
                            <ul> Licornes pour reproduction
                                @if(is_null($unicorns))
                                    <li>Aucune</li>
                                @endif
                                @foreach($unicorns as $unicorn)
                                    @if(!is_null($unicorn->firstReproDate) || !is_null($unicorn->secReproDate))
                                        <li> <a href="{{ route('unicorns.show', $unicorn->id) }}"> {{ $unicorn->name }} ({{ $unicorn->gender }})</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </p>
                    @endif
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('farms.show', $farm->idUser) }}" title="{{ $farm->user->name }}">{{ $farm->user->name }}</a></h6>
                </div>
                @if(Auth::id() == $farm->idUser)
                <div class="row justify-content-around">
                    <a class="btn btn-warning" href="{{ route('farms.edit', $farm->id) }}">modifier</a>
                    <form action="{{ route('farms.destroy') }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="id" value="{{ $farm->id }}">
                        <button class="btn btn-danger" type="submit">Supprimer</button>
                    </form>
                </div>
                @endif
            <br>
            <a href="{{ route('farms.index') }}" class="btn btn-secondary" title="Retour a la liste">Retour à la liste</a>
        </div>
        @else
        <div class="col-12">Pas d'élevage enregistré</div>
        @endif
    </div>
</div>
@endsection

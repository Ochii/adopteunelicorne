@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Modification d'un élevage <a href="{{ route('home') }}" class="btn btn-secondary" title="Retour a la home">Retour à l'accueil</a></h1>
    <form action="{{ route('farms.update', $farm->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Nom de l'élevage</label>
            <input class="form-control" id="name" type="text" name="name" value="{{ $farm->name }}">
        </div>
        <div class="form-group">
            <label for="loc">Localisation</label>
            <input class="form-control" id="loc" type="text" name="loc" value="{{ $farm->loc }}">
        </div>
        <div class="form-group">
            <label for="descr">Descriptif</label>
            <input class="form-control" id="descr" type="text" name="descr" value="{{ $farm->content }}">
        </div>
        <div class="form-group">
            <label for="idUser">Propriétaire</label>
            <select class="form-control" id="idUser" name="idUser">
            <option value="{{ $farm->idUser }}">Actuel : {{ $farm->user->name }}</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endforeach
            </select>
        </div>

        <br>
        <button class="btn btn-primary" type="submit">Enregistrer</button>
    </form>
</div>

@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <h1>Liste des élevages <a class="btn btn-success" href="{{ route('farms.create') }}">Ajouter un élevage</a></h1><br>
        </div>
    </div>
    <div class="row justify-content-center" style="height: 500px;">
        @if (!is_null($farms))
        @foreach($farms as $farm)
        <div class="col-2" style="height: 100%;">
            <div class="card" style="width: 18rem; height: 100%;">
                <div class="card-header">
                    {{ $farm->name }}
                  </div>
                <div class="card-body">
                    <h5 class="card-title">Lieu : {{ $farm->loc }}</h5>
                    <p class="card-text">Descriptif : {{ $farm->content }}</p>
                    @if(is_null($unicorns))
                        <p>pas de licorne dans cet élevage</p>
                    @else
                        <p class="card-text">
                            <p> Licornes à vendre
                                @if(!$unicorns)
                                    <li>Aucune</li>
                                @endif
                                @foreach($unicorns as $unicorn)
                                    @if(!is_null($unicorn->price) && $unicorn->idUser == $farm->idUser)
                                        <li> <a href="{{ route('unicorns.show', $unicorn->id) }}"> {{ $unicorn->name }} ({{ $unicorn->gender }})</a></li>
                                    @endif
                                @endforeach
                            </p>
                        </p>
                        <p class="card-text">
                            <p> Licornes pour reproduction
                                @if(is_null($unicorns))
                                    <li>Aucune</li>
                                @endif
                                @foreach($unicorns as $unicorn)
                                    @if(is_null($unicorn->price) && !is_null($unicorn->firstReproDate) && $unicorn->idUser == $farm->idUser)
                                        <li> <a href="{{ route('unicorns.show', '$unicorn->id') }}"> {{ $unicorn->name }} ({{ $unicorn->gender }})</a></li>
                                    @endif
                                @endforeach
                            </p>
                            <a href="{{ route('farms.show', $farm->id) }}" class="btn btn-primary">En savoir plus</a>
                        </p>
                    @endif
                </div>
                <div class="card-footer">
                    <h6 class="card-subtitle mb-2 text-muted">Propriétaire : <a href="{{ route('farms.show', $farm->idUser) }}" title="{{ $farm->user->name }}">{{ $farm->user->name }}</a></h6>
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-12">Pas d'élevage enregistré</div>
        @endif
    </div>
</div>
@endsection

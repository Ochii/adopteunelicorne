@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Les licornes à vendre</a></h1>
    </div>
    <div class="row justify-content-center">
        @if ($unicorns)
            @foreach ($unicorns as $unicorn)
            <div class="col-4">
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        {{ $unicorn->name }}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Age : {{ $unicorn->age }}</h5>
                        @if (!is_null($unicorn->price))
                            <p class="card-text">Prix de vente : {{ $unicorn->price }} €</p>
                        @else
                            <p class="card-text">Licorne uniquement pour reproduction</p>
                        @endif


                    </div>
                    <div class="card-footer">
                        <h6 class="card-subtitle mb-2 text-muted">Propriétaire : {{ $unicorn->user->name }}</h6>
                    </div>
                </div>
            </div>
            @endforeach
        @else
        <div class="col-12">Pas de licorne à vendre. Connectez vous pour en voir plus.</div>
        @endif
    </div>
</div>
@endsection

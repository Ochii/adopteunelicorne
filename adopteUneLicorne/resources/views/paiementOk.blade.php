@extends('layouts.app')

@section('content')
<div class="container">
    <h1> Félicitation !!! </h1>
    <br>
    <div class="alert alert-success" role="alert">Votre paiement a été validé</div>
    <br>
    <div>Vous êtes l'heureux propriétaire de la licorne : <span class="alert alert-info" role="alert">{{ $unicorn->name }}</span></div>
    <br>
    <a href="{{ route('unicorns.index') }}" class="btn btn-secondary" title="Retour a la liste">Retour à la liste</a>
</div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Unicorn;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PreviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->only(['preview']);
    }

    public function preview()
    {
        $unicorns = Unicorn::with('user')->whereNotNull('price')->get();
        // dd($unicorns);
        return view('preview', compact('unicorns'));
    }
}

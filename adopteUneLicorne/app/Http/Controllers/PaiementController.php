<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Unicorn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class PaiementController extends Controller
{
    public function formulaire($id) {

        $unicorn = Unicorn::find($id);
        return view ('formulairePaiement', compact('unicorn'));
    }

    public function process(Request $request) {

        Stripe::setApiKey('sk_test_51H0MmVK0PLq6qEQIGwSo5JDiBDLCNWkklZPWiH0kW1dyWuKz0P2xpe3b5NAV7tnnqk2lADCytsHmh3tngt6CQoew00yMaZrbS4');

        header('Content-Type: application/json');

        # retrieve json from POST body
        $json_str = file_get_contents('php://input');
        $json_obj = json_decode($json_str);

        $intent = null;
        try {
            if (isset($json_obj->payment_method_id)) {
                $amount = (int)$json_obj->amount * 100;
                # Create the PaymentIntent
                $intent = PaymentIntent::create([
                    'payment_method' => $json_obj->payment_method_id,
                    'confirmation_method' => 'manual',
                    'confirm' => true,
                    'amount'   => $amount,
                    'currency' => 'eur',
                    'description' => "Mon paiement"
                ]);
            }
            if (isset($json_obj->payment_intent_id)) {
                $intent = PaymentIntent::retrieve(
                    $json_obj->payment_intent_id
                );
                $intent->confirm();
            }
            if ($intent->status == 'requires_action' &&
                $intent->next_action->type == 'use_stripe_sdk') {
                # Tell the client to handle the action
                echo json_encode([
                    'requires_action' => true,
                    'payment_intent_client_secret' => $intent->client_secret
                ]);
            } else if ($intent->status == 'succeeded') {
                // Paiement Stripe accepté

                echo json_encode([
                    "success" => true
                ]);
            } else {
                http_response_code(500);
                echo json_encode(['error' => 'Invalid PaymentIntent status']);
            }
        } catch (\Exception $e) {
            # Display error on client
            echo json_encode([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function paiementOk($id) {
        //Update de la licorne
        $unicorn = Unicorn::find($id);
        $unicorn->price = null;
        $unicorn->firstReproDate = null;
        $unicorn->secReproDate = null;
        $unicorn->idUser = Auth::id();
        //Récupération de l'id de la ferme du propriétaire
        $farm = Farm::where("idUser" , Auth::id())->first();
        if($farm) {
            $unicorn->idFarm = $farm->id;
        } else {
            $unicorn->idFarm = null;
        }
        $unicorn->save();

        return view('paiementOk', compact('unicorn'));
    }
}

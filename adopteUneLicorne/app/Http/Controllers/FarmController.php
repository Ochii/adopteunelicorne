<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Unicorn;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FarmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $users = User::all();
        return view('farms.create', compact('users'));
    }

    public function store(Request $request)
    {
        $farm = new Farm();
        $farm->name = $request->get('name');
        $farm->loc = $request->get('loc');
        $farm->content = $request->get('descr');
        $farm->idUser = Auth::id();
        $farm->save();

        return redirect()->route('farms.index');
    }

    public function show($id)
    {
        $farm = Farm::with('user')->find($id);
        $unicorns = Unicorn::where('idFarm', $farm->id)->get();

        return view('farms.show', compact('farm', 'unicorns'));
    }

    public function index()
    {
        $farms = Farm::with('user')->get();
        $unicorns = Unicorn::all();

        return view('farms.index', compact('farms', 'unicorns'));
    }

    public function edit($id)
    {
        $farm = Farm::with('user')->find($id);
        $users = User::all();

        return view('farms.edit', compact('farm', 'users'));
    }

    public function update(Request $request, $id)
    {
        $farm = Farm::find($id);
        $farm->name = $request->get('name');
        $farm->loc = $request->get('loc');
        $farm->content = $request->get('descr');
        $farm->idUser = $request->get('idUser');
        $farm->save();

        return redirect()->route('farms.index');
    }

    public function destroy(Request $request)
    {
        $farm = Farm::find($request->get('id'));
        $farm->delete();

        return redirect()->route('farms.index');
    }
}

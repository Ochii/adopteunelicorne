<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Unicorn;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit($id)
    {
        if(Auth::user()->role != "admin") {
            return redirect()->route('home');
        }
        $user = User::find($id);

        return view('users.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        if(Auth::user()->role != "admin") {
            return redirect()->route('home');
        }
        $user = User::find($id);
        $user->role = $request->get('role');
        $user->save();

        return redirect()->route('users.index');
    }

    public function index()
    {
        if(Auth::user()->role != "admin") {
            return redirect()->route('home');
        }
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function destroy(Request $request)
    {
        if(Auth::user()->role != "admin") {
            return redirect()->route('home');
        }
        $user = User::find($request->get('id'));
        $user->delete();

        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = User::with('unicorns')->with('farms')->find($id);

        return view('users.show', compact('user'));
    }

    public function farms()
    {
        $farms = Farm::where('idUser', Auth::id())->get();
        $unicorns = Unicorn::with('farm')->get();

        return view('users.farms', compact('farms', 'unicorns'));
    }

    public function unicorns()
    {
        $unicorns = Unicorn::where('idUser', Auth::id())->get();

        return view('users.unicorns', compact('unicorns'));
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Farm;
use App\Models\Unicorn;
use App\User;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UnicornController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $users = User::all();
        $farms = Farm::all();
        return view('unicorns.create', compact('users', 'farms'));
    }

    public function store(Request $request)
    {
        $unicorn = new Unicorn();
        $unicorn->name = $request->get('name');
        $unicorn->age = $request->get('age');
        $unicorn->gender = $request->get('gender');
        $unicorn->price = $request->get('price');
        $unicorn->firstReproDate = $request->get('firstReproDate');
        $unicorn->secReproDate = $request->get('secReproDate');
        $unicorn->idFarm = $request->get('idFarm');
        $unicorn->idUser = Auth::id();
        $unicorn->save();

        return redirect()->route('unicorns.index');
    }

    public function show($id)
    {
        $unicorn = Unicorn::with('user')->find($id);

        return view('unicorns.show', compact('unicorn'));
    }

    public function index()
    {
        $unicornsToSell = Unicorn::with('user')->whereNotNull('price')->get();
        $unicornsToRepro = Unicorn::with('user')->whereNotNull('firstReproDate')->orWhereNotNull('secReproDate')->get();
        $unicornNoStatus = Unicorn::with('user')->whereNull('price')->whereNull('firstReproDate')->whereNull('secReproDate')->get();

        return view('unicorns.index', compact('unicornsToSell', 'unicornsToRepro', 'unicornNoStatus'));
    }

    public function edit($id)
    {
        $unicorn = Unicorn::with('farm')->find($id);
        $farms = Farm::all();
        return view('unicorns.edit', compact('unicorn', 'farms'));
    }

    public function update(Request $request, $id)
    {
        $unicorn = Unicorn::find($id);
        $unicorn->name = $request->get('name');
        $unicorn->age = $request->get('age');
        $unicorn->gender = $request->get('gender');
        $unicorn->price = $request->get('price');
        $unicorn->firstReproDate = $request->get('firstReproDate');
        $unicorn->secReproDate = $request->get('secReproDate');
        $unicorn->idFarm = $request->get('idFarm');
        $unicorn->save();

        return redirect()->route('unicorns.index');
    }

    public function destroy(Request $request)
    {
        $unicorn = Unicorn::find($request->get('id'));
        $unicorn->delete();

        return redirect()->route('unicorns.index');
    }

    public function showOnlyToSell()
    {
        $unicorns = Unicorn::with('user')->whereNotNull('price')->get();

        return view('preview', compact('unicorns'));
    }

    public function showOnlyToReproduction()
    {
        $unicorn = Unicorn::with('user')->whereNull('price')->get();

        return view('preview', compact('unicorn'));
    }
    public function showOnlyNoStatus()
    {


        return view('preview', compact('unicorn'));
    }

}

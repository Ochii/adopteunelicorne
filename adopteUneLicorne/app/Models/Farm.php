<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    protected $table = "farms";

    protected $fillable = [
        "id", "name", "loc", "content", "idUser"
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "idUser");
    }

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class);
    }
}

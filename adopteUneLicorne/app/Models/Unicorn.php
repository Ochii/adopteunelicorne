<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Unicorn extends Model
{
    protected $table = "unicorns";

    protected $fillable = [
        "id", "name", "age", "gender", "idFarm", "price", "firstReproDate", "secReproDate", "idUser"
    ];

    public function user()
    {
        return $this->belongsTo(User::class, "idUser");
    }

    public function farm()
    {
        return $this->belongsTo(Farm::class, "idFarm");
    }
}

<?php

namespace App;

use App\Models\Farm;
use App\Models\Unicorn;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Cashier;

class User extends Authenticatable
{
    use Notifiable;
    // use Billable;
    // use Cashier;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function unicorns()
    {
        return $this->hasMany(Unicorn::class, "idUser");
    }

    public function farms()
    {
        return $this->hasMany(Farm::class, "idUser");
    }
}

<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return redirect()->route('preview');
// })->middleware('guest');
// Route::get('/', 'HomeController@index')->middleware('guest')->name('home');

Route::get('/', 'PreviewController@preview')->name('preview');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/unicorns', 'UnicornController@index')->name('unicorns.index');
Route::get('/unicorns/{id}/show', 'UnicornController@show')->name('unicorns.show');
Route::get('/unicorns/create', 'UnicornController@create')->name('unicorns.create');
Route::get('/unicorns/{id}/edit', 'UnicornController@edit')->name('unicorns.edit');
Route::post('/unicorns', 'UnicornController@store')->name('unicorns.store');
Route::put('/unicorns/{id}', 'UnicornController@update')->name('unicorns.update');
Route::delete('/unicorns', 'UnicornController@destroy')->name('unicorns.destroy');

Route::get('/farms', 'FarmController@index')->name('farms.index');
Route::get('/farms/{id}/show', 'FarmController@show')->name('farms.show');
Route::get('/farms/create', 'FarmController@create')->name('farms.create');
Route::get('/farms/{id}/edit', 'FarmController@edit')->name('farms.edit');
Route::post('/farms', 'FarmController@store')->name('farms.store');
Route::put('/farms/{id}', 'FarmController@update')->name('farms.update');
Route::delete('/farms', 'FarmController@destroy')->name('farms.destroy');

Route::get('/paiement/{id}', 'PaiementController@formulaire')->name('paiement');
Route::get('/paiement-ok/{id}', 'PaiementController@paiementOK')->name('paiement-ok');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}/show', 'UserController@show')->name('users.show');
Route::put('/users/{id}', 'UserController@update')->name('users.update');
Route::delete('/users', 'UserController@destroy')->name('users.destroy');
Route::get('/users/farms', 'UserController@farms')->name('users.farms');
Route::get('/users/unicorns', 'UserController@unicorns')->name('users.unicorns');


